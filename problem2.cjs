// 2. Get all items containing only Vitamin C.

function getOnlyVitaminCItems(items){
    if(Array.isArray(items)){
        let onlyVitaminCItems = items.filter((item) => item.contains=="Vitamin C");
        onlyVitaminCItems = onlyVitaminCItems.map((item) => item.name);
        return onlyVitaminCItems;
    }
    else{
        return [];
    }
}

module.exports = getOnlyVitaminCItems;