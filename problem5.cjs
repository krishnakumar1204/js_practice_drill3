// 5. Sort items based on number of Vitamins they contain.

function sortItems(items){
    if(Array.isArray(items)){
        items.sort((item1, item2) => {
            return item2.contains.split(', ').length - item1.contains.split(', ').length;
        });

        return items;
    }
    else{
        return [];
    }
}

module.exports = sortItems;