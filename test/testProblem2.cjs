const items = require("../3-arrays-vitamins.cjs");
const getOnlyVitaminCItems = require("../problem2.cjs");

try {
    let onlyVitaminCItems = getOnlyVitaminCItems(items);
    console.log(onlyVitaminCItems);
} catch (error) {
    console.log("Something went wrong");
}