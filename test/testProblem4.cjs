const items = require("../3-arrays-vitamins.cjs");
const groupItemsOnVitamins = require("../problem4.cjs");

try {
    let groupedItems = groupItemsOnVitamins(items);
    console.log(groupedItems);
} catch (error) {
    console.log("Something went wrong");
}