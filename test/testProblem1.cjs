const items = require("../3-arrays-vitamins.cjs");
const getAvailableItems = require("../problem1.cjs");

try {
    let availableItems = getAvailableItems(items);
    console.log(availableItems);
} catch (error) {
    console.log("Something went wrong");
}