const items = require("../3-arrays-vitamins.cjs");
const sortItems = require("../problem5.cjs");

try {
    let sortedItems = sortItems(items);
    console.log(sortedItems);
} catch (error) {
    console.log("Something went wrong");
}