const items = require("../3-arrays-vitamins.cjs");
const getVitaminAItems = require("../problem3.cjs");

try {
    let vitaminAItems = getVitaminAItems(items);
    console.log(vitaminAItems);
} catch (error) {
    console.log("Something went wrong");
}