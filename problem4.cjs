// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        
//         and so on for all items and all Vitamins.
   

function groupItemsOnVitamins(items){
    if(Array.isArray(items)){
        let groupedItems = items.reduce((groupedItems, item) => {

            let vitamins = item.contains.split(', ');

            groupedItems = vitamins.reduce((groupedItems, vitamin) => {

                if(!groupedItems[vitamin]){
                    groupedItems[vitamin] = [];
                }

                groupedItems[vitamin].push(item.name);

                return groupedItems;

            }, groupedItems);

            return groupedItems;

        }, {});
        
        return groupedItems;
    }
    else{
        return [];
    }
}

module.exports = groupItemsOnVitamins;