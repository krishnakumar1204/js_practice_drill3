// 3. Get all items containing Vitamin A.

function getVitaminAItems(items){
    if(Array.isArray(items)){
        let vitaminAItems = items.filter((item) => (item.contains).includes("Vitamin A"));
        vitaminAItems = vitaminAItems.map((item) => item.name);
        return vitaminAItems;
    }
    else{
        return [];
    }
}

module.exports = getVitaminAItems;