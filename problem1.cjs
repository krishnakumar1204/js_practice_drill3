// 1. Get all items that are available

function getAvailableItems(items){
    if(Array.isArray(items)){
        let availableItems = items.filter((item) => (item.available));
        
        availableItems = availableItems.map((item) => {
            return item.name;
        });
        
        return availableItems;
    }
    else{
        return [];
    }
}

module.exports = getAvailableItems;